<?php
/**
 * Zend_Frameworkのくどさを緩和するクラス
 *
 * @author fujii@sharecoto.co.jp
 */

class Sharecoto_Helper {
    /**
     * @var self
     */
    private static $_instance;

    /**
     * @var Zend_View
     */
    private $view;

    /**
     * @var Zend_Config
     */
    private $config;

    private function __construct()
    {
        $this->view = $this->setView();
        $this->config = $this->setConfig();
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __call($name, $args)
    {
        // getHogeHogeの実装
        if (strpos($name, 'get') === 0) {
            $name = lcfirst(substr($name, 3));
            return $this->$name;
        }

        throw new BadMethodCallException;
    }

    private function setView()
    {
        $view = new Zend_View;
        $view->registerHelper(
            new Zend_View_Helper_BaseUrl,
            'baseUrl'
        );
        return $view;
    }

    private function setConfig()
    {
        $confFile = APPLICATION_PATH . '/configs/application.ini';
        return new Zend_Config_Ini($confFile, APPLICATION_ENV);
    }

    public function getAppConfig()
    {
        return $this->config->app;
    }

}
