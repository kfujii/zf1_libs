<?php
/**
 * ユーザーエージェントからjQueryのバージョンを判別して環境変数に入れます
 * iniファイルのjquery設定に依存するので注意
 */
namespace Sharecoto\Plugin;

class JQuery extends \Zend_Controller_Plugin_Abstract
{
    private $server;

    public function __construct(array $server=null)
    {
        if ($server === null) {
            $this->server = $_SERVER;
        }
        $this->server = $server;
    }

    public function dispatchLoopStartup(\Zend_Controller_Request_Abstract $request)
    {
        $this->detectJQueryVer();
    }

    public function detectJQueryVer()
    {
        $config = $this->getConfig();

        // 設定がなければなにもしない
        if (!$config) {
            return true;
        }

        $ua = $this->getUserAgent();

        // IE判定
        if ($this->isIe($ua)) {
            return $this->setJQueryVer($config['version1']);
        }

        // スマホ判定
        // 将来的にAndroid/WebKit 2.xがjquery2のサポートから外れそうなので
        if ($this->isUnderAndroid2($ua)) {
            return $this->setJQueryVer($config['version1']);
        }

        // それ以外はjQuery2系
        $this->setJQueryVer($config['version2']);

    }

    private function setJQueryVer($version)
    {
        putenv("JQUERY_VERSION={$version}");
    }

    public function getJQueryVer()
    {
        return getEnv("JQUERY_VERSION");
    }

    /**
     * UA文字列を返します
     *
     * @return string user agent 
     */
    public function getUserAgent()
    {
        $ua = $this->server['HTTP_USER_AGENT'];
        return $ua;
    }

    /**
     * IE判定
     *
     * @param string ua // user agent
     * @return boolean
     */
    public function isIe($ua)
    {
        $regex = '/MSIE ([0-9]+)/';
        if (preg_match($regex, $ua, $match)) {
            if ((integer)$match[1] <= 8) {
                return true;
            }
        };

        return false;
    }

    /**
     * Android 2以下判定
     *
     * @param string ua // user agent
     * @return boolean
     */
    public function isUnderAndroid2($ua)
    {
        if (strpos($ua, 'Android 2') !== false || strpos($ua, 'Android 1') !== false) {
            return true;
        }
        return false;
    }

    /**
     * jQuery関係の設定
     *
     * @return array
     */
    public function getConfig()
    {
        $bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
        if (is_a($bootstrap, 'Bootstrap')) {
            $config = $bootstrap->getOption('jquery');
            return $config;
        }

        $confFile = APPLICATION_PATH . '/configs/application.ini';
        $config = new \Zend_Config_Ini($confFile, APPLICATION_ENV);
        return $config->jquery->toArray();
    }

}
