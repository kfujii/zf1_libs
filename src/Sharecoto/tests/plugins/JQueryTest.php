<?php

use \Sharecoto\Plugin\JQuery as spj;

class JQueryTest extends PHPUnit_Framework_TestCase
{
    public function testInit()
    {
        $jq = new spj(array('HTTP_USER_AGENT'=>''));
    }

    /**
     * デフォルトの時
     */
    public function testDispatchLoop()
    {
        $jq = new spj(array('HTTP_USER_AGENT'=>''));

        $mock = Mockery::mock('\Zend_Controller_Request_Abstract');

        $jq->dispatchLoopStartup($mock);

        $jquery = $jq->getJqueryVer("JQUERY_VERSION");

        $this->assertEquals('2.0.3', $jquery);
    }

    /**
     * IE8のとき
     */
    public function testIE8()
    {
        $ua = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)';

        $jq = new spj(array('HTTP_USER_AGENT'=>$ua));

        $mock = Mockery::mock('\Zend_Controller_Request_Abstract');

        $jq->dispatchLoopStartup($mock);

        $jquery = $jq->getJqueryVer("JQUERY_VERSION");

        $this->assertEquals('1.10.2', $jquery);
    }

    /**
     * Android2のとき
     */
    public function testAndroid2()
    {
        $ua = 'Mozilla/5.0 (Linux; U; Android 2.3.3; ja-jp; SonyEricssonX10i Build/3.0.1.G.0.75) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1';

        $jq = new spj(array('HTTP_USER_AGENT'=>$ua));

        $mock = Mockery::mock('\Zend_Controller_Request_Abstract');

        $jq->dispatchLoopStartup($mock);

        $jquery = $jq->getJqueryVer("JQUERY_VERSION");

        $this->assertEquals('1.10.2', $jquery);
    }

}
