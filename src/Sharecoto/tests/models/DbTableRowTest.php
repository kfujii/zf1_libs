<?php

class DbTableRowTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->admin = new Application_Model_Admin;
    }

    public function testLoggingAtFetch()
    {
        $select = $this->admin->getDbTable()->select();
        $this->admin->getDbTable()->fetchAll($select);

        $log = Sharecoto_Logger::getInstance();
        $events = $log->getWriter()->events;
        $event = end($events);

        $this->assertArrayHasKey('message', $event);
    }

    public function testAppendModified()
    {
        $newPassword = md5(rand());
        $row = $this->admin->findById(1)->current();
        $currentDate = $row->modified;

        $row->password = $newPassword;
        $row->save();

        $newDate = $row->modified;

        $this->assertNotEquals($currentDate, $newDate);
    }

    public function testLoggingAtInsert()
    {
        $row = $this->admin->getDbTable()->createRow();
        $row->name = 'test_user';
        $row->password = 'password';
        $row->save();

        $log = Sharecoto_Logger::getInstance();
        $events = $log->getWriter()->events;
        $event = end($events);

        $this->assertArrayHasKey('message', $event);
    }

    public function testLoggingAtUpdate()
    {
        $row = $this->admin->findByName('test_user')->current();
        $row->password = md5(rand());
        $row->save();

        $log = Sharecoto_Logger::getInstance();
        $events = $log->getWriter()->events;
        $event = end($events);

        $this->assertArrayHasKey('message', $event);
    }

    public function testLoggingAtDelete()
    {
        $row = $this->admin->findByName('test_user')->current();
        $row->delete();

        $log = Sharecoto_Logger::getInstance();
        $events = $log->getWriter()->events;
        $event = end($events);

        $this->assertArrayHasKey('message', $event);
    }

    public function testRowsetClassIsSharecotoDbTableRowset()
    {
        $table = $this->admin->getDbTable();
        $this->assertEquals('Sharecoto_DbTable_Rowset', $table->getRowsetClass());
    }
}

