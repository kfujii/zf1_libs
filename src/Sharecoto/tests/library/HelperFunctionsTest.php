<?php
/**
 * Sharecoto\Helperは基本的にテストされているので、
 * 関数コール時に余計なエラーが出ないことだけ簡単に確認します
 */

class HelperFunctionsTest extends PHPUnit_Framework_TestCase
{
    public function testBaseUrl()
    {
        _b('test/test');
    }

    public function testEscape()
    {
        _e('<>');
    }

    public function testEscapeAndNl2br()
    {
        $str = "<\n>";
        $this->assertEquals("&lt;<br />\n&gt;", _ebr($str));
    }

    public function testGetRevision()
    {
        $rev = _getRevision();
    }
}

