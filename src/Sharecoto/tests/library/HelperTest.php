<?php

//use Sharecoto\Helper as Helper;

class HelperTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->helper = Sharecoto_Helper::getInstance();
    }

    public function testGetViewReturnZendView()
    {
        $this->assertInstanceOf('Zend_View', $this->helper->getView());
    }

    /**
     * @expectedException BadMethodCallException
     */
    public function testCatchExceptionUndefinedMethod()
    {
        $this->helper->thisMethodIsNotDefined();
    }

    public function testBaseUrl()
    {
        $test = $this->helper->getView()->baseUrl('test');
        $this->assertEquals('/test', $test);
    }

}
