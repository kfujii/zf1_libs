<?php
class JsPackTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->js = <<<JAVASCRIPT
function hello(name) { 
     // Greets the user 
     alert('Hello, ' + name); 
   } 
   hello('New user');
JAVASCRIPT;
        $this->pack = new Sharecoto_JsPack(
            $this->js
        );
    }

    public function testPack()
    {
        $compiled = 'function hello(a){alert("Hello, "+a)}hello("New user");';
        $result = $this->pack->pack();
        $this->assertEquals($compiled, $result);
    }

    public function testUtiljsPack()
    {
        $compiled = 'function hello(a){alert("Hello, "+a)}hello("New user");';
        $this->assertEquals($compiled, _jsPack($this->js));
    }
}
