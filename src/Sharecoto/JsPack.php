<?php
/**
 * Closure CompilerのREST apiを利用してJSを圧縮する。
 * Zend_CacheとZend_Http_Clientを前提にしているので注意。
 *
 * @author fujii@sharecoto.co.jp
 */

class Sharecoto_JsPack
{
    /**
     * @var string jsのコード
     */
    protected $code;

    /**
     * @var Zend_Cache
     */
    protected $cache;

    /**
     * Closure CompilerのREST apiのURL
     *
     * @var string url 
     */
    protected $endPoint = 'http://closure-compiler.appspot.com/compile';

    /**
     * Closure Compilerに渡すオプション初期設定
     *
     * @var array
     */
    protected $defaultOptions = array(
        'compilation_level' => 'SIMPLE_OPTIMIZATIONS',
        'output_format' => 'json',
        'output_info' => 'compiled_code'
    );

    /**
     * Closure Compilerに渡すオプション
     *
     * @var array
     */
    protected $options;

    /**
     * @var Zend_Http_Client
     */
    protected $client;

    /**
     * @var Sharecoto_Logger
     */
    protected $logger;

    public function __construct($code, array $options = null)
    {
        $this->code = trim($code);

        $this->options = array_merge(
            $this->defaultOptions,
            array(
                'js_code'=>$this->code
            )
        );
        if ($options) {
            $this->options = array_merge(
                $options,
                $this->defaultOptions
            );
        }

        $this->client = $this->setClient();

        $this->cache = $this->setCache();

        $this->logger  = Sharecoto_Logger::getInstance();
    }

    private function setCache()
    {
        $frontentOptions = array(
            'lifetime' => 7200,
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => '/tmp',
            'file_name_prefix' => __CLASS__,
        );

        $cache = Zend_Cache::factory(
            'Core',
            'File',
            $frontentOptions,
            $backendOptions
        );

        return $cache;
    }

    /**
     * 圧縮したコードを返します。
     * キャッシュにヒットすればキャッシュから返します。
     * ignoreErrorがtrueなら、圧縮時のエラーは無視して
     * コードをそのまま返します
     *
     * @param boolean $ignoreError.
     * @return string javascript code.
     */
    public function pack($ignoreError = false)
    {
        $cache = $this->cache;
        $key = $this->getHash($this->code);

        if (!$cache->test($key)) {
            $result = $this->closureApi();
            try {
                $this->validate($result);
            } catch (Sharecoto_JsPack_Exception $e) {
                if ($ignoreError) {
                    return $this->code;
                }
            }
            $code = json_decode($result->getBody());
            $cache->save($code->compiledCode, $key);
        }
        return $cache->load($key);
    }

    protected function closureApi()
    {
        $response = $this->client->request();

        return $response;
    }

    private function getHash($code)
    {
        return md5($code);
    }

    private function setClient()
    {
        $client = new Zend_Http_Client($this->endPoint);
        $client->setMethod(Zend_Http_Client::POST);
        $client->setParameterPost($this->options);
        return $client;
    }

    private function validate(Zend_Http_Response $response)
    {
        if ($response->isError()) {
            throw new Sharecoto_JsPack_Exception(
                $response->getMessage(),
                $response->getStatus()
            );
        }

        $body = json_decode($response->getBody());
        if (isset($body->serverErrors)) {
            $error = $body->serverErrors[0];
            throw new Sharecoto_JsPack_Exception(
                'Error.',
                $error->code
            );
        }

        if (isset($body->compiledCode) && !$body->compiledCode) {
            throw new Sharecoto_JsPack_Exception(
                'Javascript Syntax Error'
            );
        }

        return $response;
    }
}
