<?php
/**
 * Zend_Db_Tableを継承して、
 * デフォルトのrowClassを'Sharecoto_DbTable_Row'にする
 *
 * @author fujii@sharecoto.co.jp
 */

class Sharecoto_DbTable extends Zend_Db_Table_Abstract
{
    /**
     * Classname for row
     *
     * @var string
     */
    protected $_rowClass = 'Sharecoto_DbTable_Row';

    /**
     * Classname for rowset
     *
     * @var string
     */
    protected $_rowsetClass = 'Sharecoto_DbTable_Rowset';

    protected function _fetch(Zend_Db_Table_Select $select)
    {
        $log = Sharecoto_Logger::getInstance();
        $log->addDebug($select->__toString());

        return parent::_fetch($select);
    }
}
