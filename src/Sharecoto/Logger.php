<?php
/**
 * Zend_Logでクエリのログをとる
 *
 * author fujii@sharecoto.co.jp
 */

class Sharecoto_Logger
{
    /**
     * singleton
     *
     * @var self
     */
    private static $instance;

    /**
     * @var Zend_Config
     */
    private $config;

    /**
     * @var Zend_Log
     */
    private $logger;

    /**
     * Zend_Logの定数
     *
     * @var array
     */
    private $levels;

    /**
     * $_SERVER
     *
     * @var array
     */
    private $server;

    /**
     * @return void
     */
    protected function __construct($server)
    {
        $this->server = $server;

        $this->setLevels();
        $this->setConfig();
        $this->setWriter();
        $this->setLogger();
    }

    /**
     * @return Sharecoto_Logger
     */
    public static function getInstance(array $server = NULL)
    {
        if (!self::$instance) {
            if (null === $server) {
                $server = $_SERVER;
            }

            self::$instance = new self($server);
        }

        return self::$instance;
    }

    /**
     * addで始まっていたらloggingを行う。
     * それ以外のmethodが指定されていたら例外を投げる
     *
     * @return NULL
     * @throw Sharecoto_Logger_Exception
     */
    public function __call($method, $args)
    {
        // addで始まったらロギング
        if (preg_match('/^add(.*)/', $method, $matches)) {
            $level = strtoupper($matches[1]);
            if (!isset($this->levels[$level])) {
                throw new Sharecoto_Logger_Exception('Log level specified does not exist');
            }
            return $this->logger->log(json_encode($args[0]), $this->levels[$level]);
        }

        throw new Sharecoto_Logger_Exception('Undefined Method');
    }

    /**
     * @return Zend_Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * プロパティにZend_Configをセット
     */
    private function setConfig()
    {
        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini',
            APPLICATION_ENV
        );
        $this->config = $config->log;
    }

    /**
     * Zend_Logの定数を取得してプロパティにセット
     */
    private function setLevels()
    {
        $ref = new ReflectionClass('Zend_Log');
        $this->levels = $ref->getConstants();
    }

    /**
     * ログレベルの一覧を取得
     */
    public function getLevels()
    {
        return $this->levels;
    }

    /**
     * プロパティにZend_Logをセット
     */
    private function setLogger()
    {
        $writer = $this->getWriter();
        $this->logger = new Zend_Log($writer);

        // priorityをセット
        $priority = 'DEBUG'; // デフォルト
        if ($this->config->priority) {
            $priority = $this->config->priority;
        }

        $filter = new Zend_Log_Filter_Priority($this->levels[$priority]);
        $this->logger->addFilter($filter);

        // event item
        $this->logger->setEventItem('agent',
            isset($this->server['HTTP_USER_AGENT']) ? $this->server['HTTP_USER_AGENT'] : 'unknown');
        $this->logger->setEventItem('timestamp', strftime('%Y-%m-%d %H:%M:%S'));

    }

    private function setWriter()
    {
        $stream = sprintf("%s/%s_%s",
            $this->config->stream->dir,
            $this->config->stream->prefix,
            date('Ymd')
        );

        $writerClass = 'Zend_Log_Writer_' . $this->config->writer;
        $this->writer = new $writerClass($stream);

        if ($this->config->format) {
            $formatter = new Zend_Log_Formatter_Simple(
                $this->config->format . PHP_EOL
            );
            $this->writer->setFormatter($formatter);
        }
        return $this;
    }

    /**
     * zend_log_writerを取得
     */
    public function getWriter()
    {
        return $this->writer;
    }

    /**
     * @return Zend_Log
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
