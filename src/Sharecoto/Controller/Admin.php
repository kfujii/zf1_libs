<?php
/**
 * 管理ページ用Zend_Controller_Actionクラスの拡張
 *
 * @author fujii@sharecoto.co.jp
 */
class Sharecoto_Controller_Admin extends Zend_Controller_Action
{
    /**
     * Logger
     *
     * @var Sharecoto_Logger
     */
    protected $_logger;

    /**
     * server
     */
    protected $_server;

    /**
     * class constructor
     * コンストラクタの中でレイアウトを呼ぶようにしただけです
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs Any additional invocation arguments
     * @return void
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array(), array $server = null)
    {
        $this->setRequest($request)
             ->setResponse($response)
             ->_setInvokeArgs($invokeArgs);
        $this->_helper = new Zend_Controller_Action_HelperBroker($this);

        if ($server === null) {
            $this->_server = $_SERVER;
        } else {
            $this->_server = $server;
        }

        // セキュア強制
        $this->sslRequire();

        // レイアウト
        $this->_helper->layout->setLayout('layout_admin');

        // ロガー
        $this->_logger = Sharecoto_Logger::getInstance();

        $this->init();
    }

    /**
     * HTTPS判定
     *
     * @return bool
     */
    private function _isSecure()
    {
        $server = $this->_server;

        if (isset($server['HTTPS']) && $server['HTTPS'] == 'on') {
            return true;
        }
        return false;
    }

    /**
     * セキュア通信以外は403を返す
     *
     * @return void
     * @throw Exception
     */
    private function sslRequire()
    {
        // ローカルホストの時は無視する
        if ($_SERVER['HTTP_HOST'] == 'localhost') {
            return;
        }

        if (!$this->_isSecure()) {
            throw new Exception('Access forbidden', 403);
        }
    }
}
