<?php
/**
 * browscap-php(https://github.com/browscap/browscap-php)
 * をじわっと拡張
 *
 * @author fujii@sharecoto.co.jp
 */

use phpbrowscap\Browscap;

class Sharecoto_Browscap extends Browscap
{

    protected $currentBrowser;

    public function __construct($cache_dir = '/tmp', $ua=null)
    {
        parent::__construct($cache_dir);

        $this->currentBrowser = $this->getBrowser($ua);
    }

    public function isMobile()
    {
        if (null === $this->currentBrowser) {
            $this->currentBrowser = $this->getBrowser();
        }

        return $this->currentBrowser->isMobileDevice;
    }

    public function getBrowser($user_agent = null, $return_array = false)
    {
        if ($user_agent === null && $return_array = false && $this->currentBrowser) {
            return $this->currentBrowser;
        }
        return parent::getBrowser($user_agent, $return_array);
    }
}
