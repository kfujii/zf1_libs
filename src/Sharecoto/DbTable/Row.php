<?php
/**
 * Zend_Db_Tableを継承して更新系クエリ発行時に
 * ログをとる
 *
 * @author fujii@sharecoto.co.jp
 */

class Sharecoto_DbTable_Row extends Zend_Db_Table_Row_Abstract
{
    /**
     * _doInsert時に前もって評価されるクラス。
     * created, modifiedカラムが存在したら、
     * 強制的にNOW()を入れる
     */
    protected function _insert()
    {
        $info = $this->_table->info();

        $modified = $this->_modifiedFields;
        if (array_search('created', $info['cols'])) {
            $this->created = new Zend_Db_Expr('NOW()');
        }
        if (array_search('modified', $info['cols'])) {
            $this->modified = new Zend_Db_Expr('NOW()');
        }
    }

    protected function _doUpdate()
    {
        $info = $this->_table->info();
        $dataHasModified = isset($this->_modifiedFields['modified']);
        $schemeHasModified = array_search('modified', $info['cols']);

        if (!$dataHasModified && $schemeHasModified !== null) {
            $this->modified = new Zend_Db_Expr('NOW()');
        }

        parent::_doUpdate();
    }

    /**
     * INSERTが完了した時に呼ばれるメソッド
     */
    protected function _postInsert()
    {
        $message = $this->_logMessage('INSERT');
        $this->_logging($message);
    }

    /**
     * UPDATEが完了した時に呼ばれるメソッド
     */
    protected function _postUpdate()
    {
        $message = $this->_logMessage('UPDATE');
        $this->_logging($message);
    }

    /**
     * DELETEが完了した時に呼ばれるメソッド
     */
    protected function _postDelete()
    {
        $message = $this->_logMessage('DELETE');
        $this->_logging($message);
    }

    /**
     * 実際のロギング
     */
    protected function _logging(array $message)
    {
        $log = Sharecoto_Logger::getInstance();
        $log->addInfo($message);
    }

    /**
     * ログのメッセージ
     *
     * @param string $query
     * @return array
     */
    protected function _logMessage($query)
    {
        $info = $this->_getTable()->info();
        $data = $this->toArray();
        foreach ($data as $key=>$value) {
            if (is_a($value, 'Zend_Db_Expr')) {
                $data[$key] = $value->__toString();
            }
        }
        $log = array(
            'TABLE' => $info['name'],
            'QUERY' => $query,
            'DATA'  => $data,
            'CLASS' => get_class($this->_getTable())
        );

        return $log;
    }
}
